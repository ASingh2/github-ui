import Ember from 'ember';

export default Ember.Route.extend({
    model(params){
        let org=this.modelFor('org')
        return $.get(`https://api.github.com/repos/${org.id}/${params.repoid}?access_token=a754143db32cfb186e59c1fd7dbe89cc2a7d1573`)
         .then(rawRepo=>{   
            rawRepo.oldId=rawRepo.id;
            rawRepo.id=rawRepo.name;
                 return rawRepo;
         })
     }
});
