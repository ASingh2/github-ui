import Ember from 'ember';

export default Ember.Route.extend({
    model(){
        let orgId=Ember.get(this.modelFor('org'),'login');
        let repoId=Ember.get(this.modelFor('org.repo'),'name');
        return $.get(`https://api.github.com/repos/${orgId}/${repoId}/issues?access_token=a754143db32cfb186e59c1fd7dbe89cc2a7d1573`)
         .then(rawIssues=>{   
             return rawIssues.map(rawIssue=>{
                rawIssue.oldId=rawIssue.id;
                rawIssue.id=rawIssue.name;
                return rawIssue;
             })         
         })
     }
});
