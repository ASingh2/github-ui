import Ember from 'ember';

export default Ember.Route.extend({

    model(params){
        let orgId=Ember.get(this.modelFor('org'),'login');
        return $.get(`https://api.github.com/orgs/${orgId}/repos?access_token=a754143db32cfb186e59c1fd7dbe89cc2a7d1573`)
         .then(rawRepos=>{   
             return rawRepos.map(rawRepo=>{
                rawRepo.oldId=rawRepo.id;
                rawRepo.id=rawRepo.name;
                return rawRepo;
             })         
         })
     }
});
