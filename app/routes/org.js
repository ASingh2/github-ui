import Ember from 'ember';

export default Ember.Route.extend({
    actions:{
        error(jqxhr){
           if(jqxhr.status===404){
               this.intermediateTransitionTo('org.notfound')
           }else{
               return true;
           }
        }
    },
    model(params){
       return $.get(`https://api.github.com/orgs/${params.id}?access_token=a754143db32cfb186e59c1fd7dbe89cc2a7d1573`)
        .then(rawOrg=>{            
                rawOrg.oldId=rawOrg.id;
                rawOrg.id=rawOrg.name;
                return rawOrg;
        })
    }
//    authentication:Ember.inject.service(),
   
//     setupController(controller){
//         this._super(...arguments);
//         controller.set('records',this.get('authentication.records'))
//     },
//     actions:{
//         addToRecords(value){
//         this.get('authentication.records').addObject({id:value});
//     }
// }
});
