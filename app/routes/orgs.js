import Ember from 'ember';

export default Ember.Route.extend({
    favorites:Ember.inject.service(),

   model(){
       return[
           {id:"emberjs"},
           {id:"ember-cli"},
           {id:"microsoft"},
           {id:"yahoo"},
           {id:"netflix"},
           {id:"facebook"}           
       ]
   },
   actions:{
    favoriteClicked(org){
        if(this.get('favorites.items').indexOf(org)<0){
        this.get('favorites').favouriteItem(org);
    }else{
        this.get('favorites').unfavouriteItem(org);
    }
}
   }
});
