import Ember from 'ember' ;

 const {computed}=Ember;
 //another synatx
// const computed=Ember.computed;
export default function isInArray(itemKey,listKey) {
  return Ember.computed(itemKey,`${listKey}.[]`,function(){
    const item=this.get(itemKey);
    const list=this.get(listKey);
    return list.indexOf(item)>=0;
  });
}
